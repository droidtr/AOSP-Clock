package org.droidtr.clock;

import android.app.*;
import android.content.*;
import android.os.*;
import android.preference.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.android.deskclockk.*;
import com.android.deskclockk.DigitalClock;
import org.droidtr.deskclock.*;
import java.util.*;
import android.provider.*;

public class AlarmScr extends Activity {
	
    private static final String DEFAULT_SNOOZE = "2", DEFAULT_VOLUME_BEHAVIOR = "2";
	protected static final String SCREEN_OFF = "screen_off";
	protected Alarm mAlarm;
    private int mVolumeBehavior;
	
	boolean touch = false;
	float x,y,z,a,b = 1.85f;
	DisplayMetrics dm;
	int dur = 100,p,r;
	
	View bottomLayout,handleBg,snooze,dismiss,alarmTextBg;
	ImageView handle;
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
		
		mAlarm = getIntent().getParcelableExtra(Alarms.ALARM_INTENT_EXTRA);
		
		final String vol = PreferenceManager.getDefaultSharedPreferences(this).getString(
					SettingsActivity.KEY_VOLUME_BEHAVIOR,DEFAULT_VOLUME_BEHAVIOR);
					
        mVolumeBehavior = Integer.parseInt(vol);
		
		requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
					 | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		
		win.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

		if(Build.VERSION.SDK_INT >= 19)
			win.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
						 | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        if (!getIntent().getBooleanExtra(SCREEN_OFF, false)) {
            win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
						 | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
						 | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        }
		
        setContentView(R.layout.alarmscr);
		
		setTitle();
		
		handle = (ImageView) findViewById(R.id.handle);
		snooze = findViewById(R.id.snooze);
		dismiss = findViewById(R.id.dismiss);
		alarmTextBg = findViewById(R.id.alarmTextBg);
		dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		a = dm.widthPixels / 3.5f;
		p = (int)(a / 2.3f);
		r = (int)(a / 3f);
		x = handle.getX();
		snooze.setX(-(a * 0.9f));
		dismiss.setX(a * 0.9f);
		y = (a / b) * 0.15f;
		z = (a * b) * 1.04f;
		handle.setOnTouchListener(new View.OnTouchListener(){
			public boolean onTouch(View v, MotionEvent m){
				touch = (m.getAction() == m.ACTION_DOWN);
				snooze.animate().scaleX(1).scaleY(1).setDuration(dur);
				dismiss.animate().scaleX(1).scaleY(1).setDuration(dur);
				return false;
			}
		});
		bottomLayout = findViewById(R.id.bottomLayout);
		handleBg = findViewById(R.id.handleBg);
		bottomLayout.setPadding(p,p,p,p);
		handleBg.setPadding(r,r,r,r);
		alarmTextBg.setBackgroundColor(Additions.getAccentColorOrDefault(this));
		
		IntentFilter filter = new IntentFilter(Alarms.ALARM_KILLED);
        filter.addAction(Alarms.ALARM_SNOOZE_ACTION);
        filter.addAction(Alarms.ALARM_DISMISS_ACTION);
        registerReceiver(mReceiver, filter);
    }

	@Override
	public boolean onTouchEvent(MotionEvent event){
		if(touch){
				if((event.getAction() & event.ACTION_MASK) == event.ACTION_DOWN){
					b = (event.getX()-a);
					if(b >= y && b <= z){
						handle.setX(b);
						handle.setImageResource(R.drawable.alarm_handle);
						//handle.setAlpha(255);
					} /*else if(b < y || b > z){
						handle.setAlpha(0);
					}*/
				}
				else if((event.getAction() & event.ACTION_MASK) == event.ACTION_UP){
					if((event.getX()-a) >= z)
						dismiss(false);
					else if((event.getX()-a) <= y)
						snooze();
					else {
						snooze.animate().scaleX(0).scaleY(0).setDuration(dur);
						dismiss.animate().scaleX(0).scaleY(0).setDuration(dur);
						handle.setImageResource(R.drawable.clock);
						//handle.setAlpha(255);
					}
					handle.animate().x(x+a);
					touch = false;
				}
		}
		return super.onTouchEvent(event);
	}
	
    private void dismiss(boolean killed) {
        if(!killed){
            NotificationManager nm = getNotificationManager();
            nm.cancel(mAlarm.id);
            stopService(new Intent(Alarms.ALARM_ALERT_ACTION).setPackage(getPackageName()));
        }
        t(getString(R.string.alarm_dismissed));
    }
	
	private NotificationManager getNotificationManager() {
        return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }
	
	private void snooze(){
        if (!findViewById(R.id.snooze).isEnabled()) {
            dismiss(false);
            return;
        }
        final String snooze =
			PreferenceManager.getDefaultSharedPreferences(this)
			.getString(SettingsActivity.KEY_ALARM_SNOOZE, DEFAULT_SNOOZE);
        int snoozeMinutes = Integer.parseInt(snooze);

        final long snoozeTime = System.currentTimeMillis()
			+ (1000 * 60 * snoozeMinutes);
        Alarms.saveSnoozeAlert(this, mAlarm.id,
							   snoozeTime);
							   
        final Calendar c = Calendar.getInstance();
        c.setTimeInMillis(snoozeTime);

        String label = mAlarm.getLabelOrDefault(this);
        label = getString(R.string.alarm_notify_snooze_label, label);

        Intent cancelSnooze = new Intent(this, AlarmReceiver.class);
        cancelSnooze.setAction(Alarms.CANCEL_SNOOZE);
        cancelSnooze.putExtra(Alarms.ALARM_ID, mAlarm.id);
        PendingIntent broadcast =
			PendingIntent.getBroadcast(this, mAlarm.id, cancelSnooze, 0);
		NotificationManager nm = getNotificationManager();
		Notification n = new Notification();
		n.icon = R.drawable.iclock;
		Bundle b = n.extras;
		b.putString(n.EXTRA_TITLE,label);
		b.putString(n.EXTRA_TEXT,getString(R.string.alarm_notify_snooze_text,
										   Alarms.formatTime(this, c)));
		b.putBoolean(n.EXTRA_SHOW_WHEN,true);
		n.contentIntent = broadcast;
		n.flags |= Notification.FLAG_AUTO_CANCEL
			| Notification.FLAG_ONGOING_EVENT;
        nm.notify(mAlarm.id, n);

        String displayTime = getString(R.string.alarm_alert_snooze_set,
									   snoozeMinutes);
       
        t(displayTime);
        stopService(new Intent(Alarms.ALARM_ALERT_ACTION).setPackage(getPackageName()));
    }
	
	Handler h = new Handler();
	
	public void t(String s){
		TextView tv = (TextView) findViewById(R.id.alarmClosed);
		tv.setText(s);
		tv.animate().scaleX(1).scaleY(1).setDuration(dur);
		h.postDelayed(new Runnable(){
			public void run(){
				finish();
			}
		},2500);
	}
	
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Alarms.ALARM_SNOOZE_ACTION)) {
                snooze();
            } else if (action.equals(Alarms.ALARM_DISMISS_ACTION)) {
                dismiss(false);
            } else {
                Alarm alarm = intent.getParcelableExtra(Alarms.ALARM_INTENT_EXTRA);
                if (alarm != null && mAlarm.id == alarm.id) {
                    dismiss(true);
                }
            }
        }
    };
	
	private void setTitle() {
        String label = mAlarm.getLabelOrDefault(this);
        TextView title = (TextView) findViewById(R.id.alarmText);
        title.setText(label);
		((DigitalClock) findViewById(R.id.time)).setLive(false);
    }
	
	public String getHM(){
		Date d = new Date();
		return d.getHours()+":"+d.getMinutes();
	}

	@Override
	public void onBackPressed(){
		return;
	}
	
	@Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mAlarm = intent.getParcelableExtra(Alarms.ALARM_INTENT_EXTRA);
        setTitle();
    }
	
	@Override
    protected void onResume() {
        super.onResume();
        if (Alarms.getAlarm(getContentResolver(), mAlarm.id) == null)
			if(snooze != null) snooze.setEnabled(false);
    }
	
	@Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }
	
	@Override
    public boolean dispatchKeyEvent(KeyEvent event){
        switch (event.getKeyCode()){
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_CAMERA:
            case KeyEvent.KEYCODE_FOCUS:
                if(event.getAction() == KeyEvent.ACTION_UP){
                    switch(mVolumeBehavior){
                        case 1:
                            snooze();
                            break;
                        case 2:
                            dismiss(false);
                            break;
                        default:
                            break;
                    }
                }
                return true;
            default:
                break;
        }
        return super.dispatchKeyEvent(event);
    }
}
