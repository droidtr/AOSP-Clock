package org.droidtr.clock;

import android.content.*;
import android.graphics.drawable.*;
import android.provider.*;
import android.service.quicksettings.*;
import android.util.*;
import com.android.deskclockk.*;
import org.droidtr.deskclock.R;

import android.util.Log;
import com.android.deskclockk.AlarmClock;
import android.text.*;
import java.io.*;

public class AlarmTile extends TileService {

	String tileText = null;
	Tile t = null;
	
	boolean textIsNotEmpty(){
		try {
			return !TextUtils.isEmpty(tileText);
		} catch(Exception | Error e){
			Log.e("TileService",e.toString(),e.getCause());
			return false;
		}
	}
	
	void cmds(){
		if(t == null) initTile();
		readDetails();
	}
	
	void initTile(){
		t = getQsTile();
		t.setState(Tile.STATE_ACTIVE);
		t.setIcon(Icon.createWithResource(this,R.drawable.iclock));
	}
	
	void readDetails(){
		tileText = Settings.System.getString(
			getContentResolver(),Settings.System.NEXT_ALARM_FORMATTED);
		t.setLabel(textIsNotEmpty() ? tileText : getString(R.string.add_alarm));
		t.updateTile();
	}

	@Override
	public void onClick(){
		startActivity(new Intent(this,textIsNotEmpty() ? AlarmClock.class : SetAlarm.class));
		super.onClick();
	}

	@Override
	public void onStartListening(){
		cmds();
		super.onStartListening();
	}
}
