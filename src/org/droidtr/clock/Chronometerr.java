package org.droidtr.clock;

import android.app.*;
import android.icu.text.*;
import android.os.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import com.android.deskclockk.*;

import org.droidtr.deskclock.R;
import android.util.*;
import android.graphics.*;

public class Chronometerr extends Activity {
	ListView lv;
	ArrayAdapter<Spanned> la;
	final int pp = 0,tr = 1,rs = 2,cc = 3,pc = 4;
	boolean p = true,r = true;
	long lct = 0,lst = 0,lmt = 0,let = 0,ldt = 0,lft = 0,lxt = 0;
	SimpleDateFormat sdf;
	TextView tv;
	String space = "\t\t\t";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chronometer);
		lv = (ListView) findViewById(android.R.id.list);
		la = new ArrayAdapter<Spanned>(this, android.R.layout.simple_list_item_1){
			@Override
			public View getView(int p1, View p2, ViewGroup p3){
				TextView v = (TextView) super.getView(p1,p2,p3);
				v.setGravity(Gravity.CENTER);
				v.setTextSize(TypedValue.COMPLEX_UNIT_DIP,24);
				v.setTypeface(Typeface.create("sans-serif-light",0));
				return v;
			}
		};
		lv.setAdapter(la);
		sdf = new SimpleDateFormat("mm:ss.-SS,");
		tv = (TextView) findViewById(R.id.chronometerText);
		setTxt(0);
		((LinearLayout) findViewById(R.id.chrll)).setMotionEventSplittingEnabled(false);
    }

	Handler h = new Handler(){
		@Override
		public void handleMessage(Message msg){
			switch(msg.what){
				case pp:
					if(r && lmt == 0 || p)
						ldt = lst = System.currentTimeMillis();
					r = false;
					p = !p;
					h.removeMessages(pp);
					h.sendEmptyMessage(p ? pc : cc);
					break;
				case tr:
					insTxt();
					ldt = System.currentTimeMillis();
					lft = 0;
					h.removeMessages(tr);
					break;
				case rs:
					h.removeMessages(cc);
					lv.removeAllViewsInLayout();
					la.clear();
					ldt = let = lft = lmt = lst = lct = 0;
					if(x != null){
						x.setTag(1);
						x.setImageResource(R.drawable.start);
					}
					p = r = true;
					h.removeMessages(rs);
					setTxt(0);
					h.removeMessages(pc);
					h.sendEmptyMessage(pc);
					break;
				case cc:
					h.removeMessages(cc);
					if(!p){
						if(!r) h.sendEmptyMessageDelayed(cc,10);
						lct = (System.currentTimeMillis() - lst) + lmt;
						let = (System.currentTimeMillis() - ldt) + lft;
						setTxt(lct);
					}
					break;
				case pc:
					p = true;
					h.removeMessages(pc);
					lmt = lct;
					lft = let;
					ldt = let = lct = lst = 0;
					break;
			}
			super.handleMessage(msg);
		}
	};
	
	private void insTxt(){
		lxt = la.getCount();
		la.insert(Html.fromHtml((lxt + 1) + space + 
				(lxt > 0 ? txt(let) + space : "") + txt(lct)),0);
	}
	
	private String txt(long l){
		return sdf.format(l).replace("-","<small>").replace(",","</small>");
	}
	
	private void setTxt(long l){
		tv.setText(Html.fromHtml(txt(l)));
	}
	
	/*private String zf(long i){
		return i > 9 ? ""+i : "0"+i;
	}*/
	
	ImageButton x;

	public void init(View v){
		v.setTag(v.getTag().equals(0) ? 1 : 0);
		(x = (ImageButton)v).setImageResource(v.getTag().equals(0) ? R.drawable.pause : R.drawable.start);
		h.sendEmptyMessage(pp);
	}

	public void tour(View v){
		if(!p) h.sendEmptyMessage(tr);
	}

	public void reset(View v){
		if(!r) h.sendEmptyMessage(rs);
	}
}
