package org.droidtr.clock;

import android.content.*;
import android.net.*;
import android.os.*;
import android.provider.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.android.music.*;
import java.lang.reflect.*;

public class Additions {
	public static boolean detectNavbar(Context context){
		if(Build.VERSION.SDK_INT >= 14){
			try {
				Class<?> serviceManager = Class.forName("android.os.ServiceManager");
				IBinder serviceBinder = (IBinder)serviceManager.getMethod("getService", String.class).invoke(serviceManager, "window");
				Class<?> stub = Class.forName("android.view.IWindowManager$Stub");
				Object windowManagerService = stub.getMethod("asInterface", IBinder.class).invoke(stub, serviceBinder);
				Method hasNavigationBar = windowManagerService.getClass().getMethod("hasNavigationBar");
				return (boolean) hasNavigationBar.invoke(windowManagerService);
			} catch(Exception e){
				return ViewConfiguration.get(context).hasPermanentMenuKey();
			}
		}
		return (!(KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK) && 
			KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME)));
	}
	
	public static void requestForDisableDoze(Context context){
		if(Build.VERSION.SDK_INT >= 23){
			Intent intent = new Intent();
			String packageName = context.getPackageName();
			PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			if (!pm.isIgnoringBatteryOptimizations(packageName)){
				intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
				intent.setData(Uri.parse("package:" + packageName));
				context.startActivity(intent);
			}
		}
	}
	
	public static int getAccentColorOrDefault(Context c){
		if(Build.VERSION.SDK_INT >= 20){
			TypedValue typedValue = new TypedValue();
			c.getTheme().resolveAttribute(android.R.attr.colorAccent, typedValue, true);
			return typedValue.data - 0x55000000;
		}
		return 0xAA99BBFF;
	}
	
	public static Intent getMusicService(){
		Intent it = new Intent(getServiceName());
		it.setComponent(getComponentName());
		return it;
	}
	
	private static String getServiceName(){
			return "com.android.music.IMediaPlaybackService";
	}
	
	private static ComponentName getComponentName(){
		return new ComponentName("com.keskin.music", "com.android.music.MediaPlaybackService");
	}
	
	public static final String getPrevious(){
		return "com.android.music.musicservicecommand.previous";
	}
	
	public static final String getPlayPause(){
		return "com.android.music.musicservicecommand.togglepause";
	}
	
	public static final String getNext(){
		return "com.android.music.musicservicecommand.next";
	}
	
	public static final String getPlayStateChanged(){
		return "com.android.music.playstatechanged";
	}
	
	public static final String getMetaChanged(){
		return "com.android.music.metachanged";
	}
	
	public static final String getQueueChanged(){
		return "com.android.music.queuechanged";
	}
}
