/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.deskclockk;

import android.app.*;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.os.*;
import android.provider.*;
import android.text.*;
import android.text.format.*;
import android.util.*;
import android.view.*;
import android.view.animation.*;
import android.widget.*;
import java.util.*;
import org.droidtr.clock.*;
import org.droidtr.deskclock.R;

import android.util.Log;

import static android.os.BatteryManager.*;
import com.android.music.*;

/**
 * DeskClock clock view for desk docks.
 */
public class DeskClock extends Activity {

    private static final String LOG_TAG = "DeskClock";

    // Alarm action for midnight (so we can update the date display).
    private static final String ACTION_MIDNIGHT = "com.android.deskclockk.MIDNIGHT";

    // Delay before engaging the burn-in protection mode (green-on-black).
    private final long SCREEN_SAVER_TIMEOUT = /*5 * */60 * 1000; // 5 min (if comment is uncommented)

    // Repositioning delay in screen saver.
    private final long SCREEN_SAVER_MOVE_DELAY = 60 * 1000; // 1 min

    // Color to use for text & graphics in screen saver mode.
    private final int SCREEN_SAVER_COLOR = 0xFF888888;
    private final int SCREEN_SAVER_COLOR_DIM = 0xFF444444;

    // Opacity of black layer between clock display and wallpaper.
    private final float DIM_BEHIND_AMOUNT_NORMAL = 0.4f;
    private final float DIM_BEHIND_AMOUNT_DIMMED = 0.8f; // higher contrast when display dimmed

    // Internal message IDs.
    private final int SCREEN_SAVER_TIMEOUT_MSG   = 0x2000;
    private final int SCREEN_SAVER_MOVE_MSG      = 0x2001;
	
	private final int MUSIC_PLAYER_CONNECTED     = 0x2002;
	private final int MUSIC_PLAYER_DISCONNECTED  = 0x2003;
	private final int MUSIC_PLAYER_REFRESH       = 0x2004;

    // State variables follow.
    private DigitalClock mTime;

    private TextView mDate,mNextAlarm = null,mBatteryDisplay,
						mNowPlayingTrack,mNowPlayingArtist;
	
	private View playerView,prevBtn,ppBtn,nextBtn,albumArt,albumArtFg;
	

    private boolean mDimmed = false;
    private boolean mScreenSaverMode = false;
    private String mDateFormat;

    private int mBatteryLevel = -1;
    private boolean mPluggedIn = false;

    private boolean mLaunchedFromDock = false;
	private boolean musicPlayerConnected = false;

    private Random mRNG;

    private PendingIntent mMidnightIntent;
	
	private String track,aalbum;
	private boolean isPlaying;
	private Bitmap albumArtBmp;
	
    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (Intent.ACTION_DATE_CHANGED.equals(action) || ACTION_MIDNIGHT.equals(action)) {
                refreshDate();
            } else if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
                handleBatteryUpdate(
                    intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0),
                    intent.getIntExtra(BatteryManager.EXTRA_STATUS, BATTERY_STATUS_UNKNOWN),
                    intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0));
            } else if (UiModeManager.ACTION_EXIT_DESK_MODE.equals(action)) {
                if (mLaunchedFromDock) {
                    // moveTaskToBack(false);
                    finish();
                }
                mLaunchedFromDock = false;
            }
        }
    };

    private final Handler mHandy = new Handler() {
        @Override
        public void handleMessage(Message m) {
			switch(m.what){
				case SCREEN_SAVER_TIMEOUT_MSG:
					saveScreen();
					break;
				case SCREEN_SAVER_MOVE_MSG:
					moveScreenSaver();
					break;
				case MUSIC_PLAYER_CONNECTED:
					mHandy.removeMessages(MUSIC_PLAYER_CONNECTED);
					setMusicPlayerView(musicPlayerConnected = true);
					break;
				case MUSIC_PLAYER_DISCONNECTED:
					mHandy.removeMessages(MUSIC_PLAYER_DISCONNECTED);
					setMusicPlayerView(musicPlayerConnected = false);
					break;
				case MUSIC_PLAYER_REFRESH:
					mHandy.removeMessages(MUSIC_PLAYER_REFRESH);
					setMusicPlayerView(musicPlayerConnected);
					break;
			}
        }
    };

	private void setMusicPlayerView(boolean enabled){
		playerView.setVisibility(enabled ? View.VISIBLE : View.GONE);
		if(enabled){
			try {
				track = imps.getTrackName();
				aalbum = imps.getArtistName()+" - "+imps.getAlbumName();
				isPlaying = imps.isPlaying();
				albumArtBmp = imps.getAlbumArtBitmap(false);
				mNowPlayingTrack.setText(track);
				mNowPlayingArtist.setText(aalbum);
				((ImageView)ppBtn).setImageResource(isPlaying ? 
									R.drawable.lb_ic_pause : R.drawable.lb_ic_play);
				albumArt.setVisibility(isPlaying ? View.VISIBLE : View.GONE);
				albumArtFg.setVisibility(isPlaying ? View.VISIBLE : View.GONE);
				((ImageView)albumArt).setImageBitmap(albumArtBmp);
			} catch(Exception e){}
		}
	}
	
	private BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
			mHandy.sendEmptyMessage(MUSIC_PLAYER_REFRESH);
        }
    };

    private void moveScreenSaver() {
        moveScreenSaverTo(-1,-1);
    }
	
    private void moveScreenSaverTo(int x, int y) {
        if (!mScreenSaverMode) return;

        final View saver_view = findViewById(R.id.saver_view);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        if (x < 0 || y < 0) {
            int myWidth = saver_view.getMeasuredWidth();
            int myHeight = saver_view.getMeasuredHeight();
            x = (int)(mRNG.nextFloat()*(metrics.widthPixels - myWidth));
            y = (int)(mRNG.nextFloat()*(metrics.heightPixels - myHeight));
        }

        saver_view.setLayoutParams(new AbsoluteLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            x,
            y));

        // Synchronize our jumping so that it happens exactly on the second.
        mHandy.sendEmptyMessageDelayed(SCREEN_SAVER_MOVE_MSG,
            SCREEN_SAVER_MOVE_DELAY +
            (1000 - (System.currentTimeMillis() % 1000)));
    }

    private void setWakeLock(boolean hold) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.flags |= (WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        if (hold)
            winParams.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        else
            winParams.flags &= (~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        win.setAttributes(winParams);
    }

    private void scheduleScreenSaver() {
        // reschedule screen saver
        mHandy.removeMessages(SCREEN_SAVER_TIMEOUT_MSG);
        mHandy.sendMessageDelayed(
            Message.obtain(mHandy, SCREEN_SAVER_TIMEOUT_MSG),
            SCREEN_SAVER_TIMEOUT);
    }

    private void restoreScreen() {
        if (!mScreenSaverMode) return;
        mScreenSaverMode = false;
        initViews();
        doDim(false); // restores previous dim mode

        scheduleScreenSaver();

        refreshAll();
    }

    // Special screen-saver mode for OLED displays that burn in quickly
    private void saveScreen() {
        if (mScreenSaverMode) return;

        // quickly stash away the x/y of the current date
        final View oldTimeDate = findViewById(R.id.time_date);
        int oldLoc[] = new int[2];
        oldTimeDate.getLocationOnScreen(oldLoc);

        mScreenSaverMode = true;
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        win.setAttributes(winParams);
        // give up any internal focus before we switch layouts
        final View focused = getCurrentFocus();
        if (focused != null) focused.clearFocus();

        setContentView(R.layout.desk_clock_saver);

        mTime = (DigitalClock) findViewById(R.id.time);
        mDate = (TextView) findViewById(R.id.date);
        mNextAlarm = (TextView) findViewById(R.id.nextAlarm);

        final int color = mDimmed ? SCREEN_SAVER_COLOR_DIM : SCREEN_SAVER_COLOR;

		hideNavbar();
		
        ((TextView)findViewById(R.id.timeDisplay)).setTextColor(color);
        ((TextView)findViewById(R.id.am_pm)).setTextColor(color);
        mDate.setTextColor(color);
        mNextAlarm.setTextColor(color);
        mNextAlarm.setCompoundDrawablesWithIntrinsicBounds(
            getResources().getDrawable(mDimmed
                ? R.drawable.mclock
                : R.drawable.gclock),
            null, null, null);

        mBatteryDisplay = null;

        refreshDate();
        refreshAlarm();

        moveScreenSaverTo(oldLoc[0], oldLoc[1]);
    }

    @Override
    public void onUserInteraction() {
        if (mScreenSaverMode)
            restoreScreen();
    }

    // Adapted from KeyguardUpdateMonitor.java
    private void handleBatteryUpdate(int plugged, int status, int level) {
        final boolean pluggedIn = (plugged != 0);
        if (pluggedIn != mPluggedIn) {
            setWakeLock(pluggedIn);
        }
        if (pluggedIn != mPluggedIn || level != mBatteryLevel) {
            mBatteryLevel = level;
            mPluggedIn = pluggedIn;
            refreshBattery();
        }
    }

    private void refreshBattery() {
        if (mBatteryDisplay == null) return;

        if (mPluggedIn) {
            mBatteryDisplay.setCompoundDrawablesWithIntrinsicBounds(
                0, 0, R.drawable.flash, 0);
            mBatteryDisplay.setText(
                getString(R.string.battery_charging_level, mBatteryLevel));
            mBatteryDisplay.setVisibility(View.VISIBLE);
        } else {
            mBatteryDisplay.setVisibility(View.INVISIBLE);
        }
    }

    private void refreshDate() {
        final Date now = new Date();
        mDate.setText(DateFormat.format(mDateFormat, now));
    }

    private void refreshAlarm() {
        if (mNextAlarm == null) return;

        String nextAlarm = Settings.System.getString(getContentResolver(),
                Settings.System.NEXT_ALARM_FORMATTED);
        if (!TextUtils.isEmpty(nextAlarm)) {
            mNextAlarm.setText(nextAlarm);
            //mNextAlarm.setCompoundDrawablesWithIntrinsicBounds(
            //    android.R.drawable.ic_lock_idle_alarm, 0, 0, 0);
            mNextAlarm.setVisibility(View.VISIBLE);
        } else {
            mNextAlarm.setVisibility(View.INVISIBLE);
        }
    }

    private void refreshAll() {
        refreshDate();
        refreshAlarm();
        refreshBattery();
    }

    private void doDim(boolean fade) {
        View tintView = findViewById(R.id.window_tint);
        if (tintView == null) return;
		hideNavbar();
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();

        winParams.flags |= (WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        winParams.flags |= (WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        // dim the wallpaper somewhat (how much is determined below)
        winParams.flags |= (WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        if (mDimmed) {
            winParams.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
            winParams.dimAmount = DIM_BEHIND_AMOUNT_DIMMED;
            winParams.buttonBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_OFF;
			
            // show the window tint
            tintView.startAnimation(AnimationUtils.loadAnimation(this,
                fade ? R.anim.dim
                     : R.anim.dim_instant));
        } else {
            winParams.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
            winParams.dimAmount = DIM_BEHIND_AMOUNT_NORMAL;
            winParams.buttonBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
			
            // hide the window tint
            tintView.startAnimation(AnimationUtils.loadAnimation(this,
                fade ? R.anim.undim
                     : R.anim.undim_instant));
        }

        win.setAttributes(winParams);
		
		mHandy.sendEmptyMessage(MUSIC_PLAYER_REFRESH);
    }

    @Override
    public void onNewIntent(Intent newIntent) {
        super.onNewIntent(newIntent);

        // update our intent so that we can consult it to determine whether or
        // not the most recent launch was via a dock event 
        setIntent(newIntent);
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_DATE_CHANGED);
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        filter.addAction(UiModeManager.ACTION_EXIT_DESK_MODE);
        filter.addAction(ACTION_MIDNIGHT);
        registerReceiver(mIntentReceiver, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterReceiver(mIntentReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();

        // reload the date format in case the user has changed settings
        // recently
        mDateFormat = getString(R.string.full_wday_month_day_no_year);

        // Elaborate mechanism to find out when the day rolls over
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.add(Calendar.DATE, 1);
        long alarmTimeUTC = today.getTimeInMillis();

        mMidnightIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_MIDNIGHT), 0);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC, alarmTimeUTC, AlarmManager.INTERVAL_DAY, mMidnightIntent);
        

        // If we weren't previously visible but now we are, it's because we're
        // being started from another activity. So it's OK to un-dim.
        if (mTime != null && mTime.getWindowVisibility() != View.VISIBLE) {
            mDimmed = false;
        }

        // Adjust the display to reflect the currently chosen dim mode.
        doDim(false);

        restoreScreen(); // disable screen saver
        refreshAll(); // will schedule periodic weather fetch

        setWakeLock(mPluggedIn);

        scheduleScreenSaver();

        final boolean launchedFromDock
            = getIntent().hasCategory(Intent.CATEGORY_DESK_DOCK);

        mLaunchedFromDock = launchedFromDock;
		
		try {
			bindService(Additions.getMusicService(),sc,BIND_AUTO_CREATE);
		} catch(Exception e){}
    }

    @Override
    public void onPause() {

        // Turn off the screen saver and cancel any pending timeouts.
        // (But don't un-dim.)
        mHandy.removeMessages(SCREEN_SAVER_TIMEOUT_MSG);
        restoreScreen();

        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        am.cancel(mMidnightIntent);
		
        super.onPause();
    }

    private void initViews() {
        // give up any internal focus before we switch layouts
        final View focused = getCurrentFocus();
        if (focused != null) focused.clearFocus();

        setContentView(R.layout.desk_clock);

        mTime = (DigitalClock) findViewById(R.id.time);
        mDate = (TextView) findViewById(R.id.date);
        mBatteryDisplay = (TextView) findViewById(R.id.battery);
		
		playerView = findViewById(R.id.nowplaying);
		mNowPlayingTrack = (TextView) findViewById(R.id.title);
		mNowPlayingArtist = (TextView) findViewById(R.id.artist);
		
		prevBtn = findViewById(R.id.npprev);
		ppBtn = findViewById(R.id.npplay);
		nextBtn = findViewById(R.id.npnext);
		albumArt = findViewById(R.id.icon);
		albumArtFg = findViewById(R.id.iconfg);
		
		prevBtn.setOnClickListener(mediaControl);
		ppBtn.setOnClickListener(mediaControl);
		nextBtn.setOnClickListener(mediaControl);
		
		mRNG = new Random();
		
        mTime.getRootView().requestFocus();

        final View.OnClickListener alarmClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(DeskClock.this, AlarmClock.class));
            }
        };

        mNextAlarm = (TextView) findViewById(R.id.nextAlarm);
        mNextAlarm.setOnClickListener(alarmClickListener);

        final ImageButton alarmButton = (ImageButton) findViewById(R.id.alarm_button);
        alarmButton.setOnClickListener(alarmClickListener);

        final ImageButton chronometerButton = (ImageButton) findViewById(R.id.chronometer_button);
        chronometerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    startActivity(new Intent(DeskClock.this,Chronometerr.class));
                } catch (android.content.ActivityNotFoundException e) {
                    Log.e(LOG_TAG, "Couldn't launch image browser", e);
                }
            }
        });

        final ImageButton homeButton = (ImageButton) findViewById(R.id.home_button);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(
                    new Intent(Intent.ACTION_MAIN)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addCategory(Intent.CATEGORY_HOME));
            }
        });
		
		final ImageButton settingsButton = (ImageButton) findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(DeskClock.this, SettingsActivity.class));
			}
		});

		final View deskClockLayout = findViewById(R.id.deskClockLayout);
        deskClockLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mDimmed = ! mDimmed;
                doDim(true);
            }
        });

        deskClockLayout.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                saveScreen();
                return true;
            }
        });

        final View tintView = findViewById(R.id.window_tint);
        tintView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (mDimmed && event.getAction() == MotionEvent.ACTION_DOWN) {
                    // We want to un-dim the whole screen on tap.
                    // ...Unless the user is specifically tapping on the dim
                    // widget, in which case let it do the work.
                    Rect r = new Rect();
                    deskClockLayout.getHitRect(r);
                    int[] gloc = new int[2];
                    deskClockLayout.getLocationInWindow(gloc);
                    r.offsetTo(gloc[0], gloc[1]); // convert to window coords
					
                    if (!r.contains((int) event.getX(), (int) event.getY())) {
                        mDimmed = false;
                        doDim(true);
                    }
                }
                return false; // always pass the click through
            }
        });

        // Tidy up awkward focus behavior: the first view to be focused in
        // trackball mode should be the alarms button
        final ViewTreeObserver vto = alarmButton.getViewTreeObserver();
        vto.addOnGlobalFocusChangeListener(new ViewTreeObserver.OnGlobalFocusChangeListener() {
            public void onGlobalFocusChanged(View oldFocus, View newFocus) {
                if (oldFocus == null && newFocus == deskClockLayout) {
                    alarmButton.requestFocus();
                }
            }
        });
    }
	
	private void hideNavbar(){
		if(Additions.detectNavbar(this) && Build.VERSION.SDK_INT >= 16)
			mTime.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | 
										View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
										View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
	}

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
		hideNavbar();
        if (mScreenSaverMode) {
            moveScreenSaver();
        } else {
            initViews();
            doDim(false);
            refreshAll();
        }
    }

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
		Additions.requestForDisableDoze(this);
        initViews();
    }
	
	IMediaPlaybackService imps;

	ServiceConnection sc = new ServiceConnection(){
		@Override
		public void onServiceConnected(ComponentName p1, IBinder p2){
			imps = IMediaPlaybackService.Stub.asInterface(p2);
			IntentFilter i = new IntentFilter(Additions.getPlayStateChanged());
			i.addAction(Additions.getMetaChanged());
			i.addAction(Additions.getQueueChanged());
			registerReceiver(mStatusListener,i);
			musicPlayerConnected = true;
			mHandy.sendEmptyMessage(MUSIC_PLAYER_CONNECTED);
		}

		@Override
		public void onServiceDisconnected(ComponentName p1){
			imps = null;
			unregisterReceiver(mStatusListener);
			musicPlayerConnected = false;
			mHandy.sendEmptyMessage(MUSIC_PLAYER_DISCONNECTED);
		}
	};
	
	private View.OnClickListener mediaControl = new View.OnClickListener(){
		@Override
		public void onClick(View v){
			Intent i = new Intent();
			switch(v.getId()){
				case R.id.npprev:
					i.setAction(Additions.getPrevious());
					break;
				case R.id.npplay:
					i.setAction(Additions.getPlayPause());
					break;
				case R.id.npnext:
					i.setAction(Additions.getNext());
					break;
				default:
					return;
			}
			sendBroadcast(i);
		}
	};
}
