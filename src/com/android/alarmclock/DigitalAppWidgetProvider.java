package com.android.alarmclock;

import com.android.deskclockk.AlarmClock;
import org.droidtr.deskclock.R;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.util.*;
import android.os.*;
import android.appwidget.*;

/**
 * Simple widget to show digital clock.
 */
public class DigitalAppWidgetProvider extends BroadcastReceiver {
	
    public void onReceive(Context context, Intent intent) {
        if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(intent.getAction())) {
            RemoteViews views = new RemoteViews(context.getPackageName(),
                    R.layout.digital_appwidget);

            views.setOnClickPendingIntent(R.id.digital_appwidget,
                    PendingIntent.getActivity(context, 0,
                        new Intent(context, AlarmClock.class),
                        PendingIntent.FLAG_CANCEL_CURRENT));

            AppWidgetManager.getInstance(context).
								updateAppWidget(intent.getIntArrayExtra(
									AppWidgetManager.EXTRA_APPWIDGET_IDS), views);
        }
    }
}

