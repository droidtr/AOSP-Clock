SDK=$(HOME)/Android/Sdk
TARGET=28
TOOL=28.0.3
JAVADIR=$(JAVA_HOME)/bin
BUILDTOOLS=$(SDK)/build-tools/$(TOOL)
AJAR=$(SDK)/platforms/android-$(TARGET)/android.jar
ADX=$(BUILDTOOLS)/dx
AAPT=$(BUILDTOOLS)/aapt
JAVAC=$(JAVADIR)/javac
JFLAG=-source 8 -Xlint:deprecation
JARSIGNER=$(JAVADIR)/jarsigner
APKSIGNER=$(BUILDTOOLS)/apksigner
ZIPALIGN=$(BUILDTOOLS)/zipalign
KEYTOOL=$(JAVADIR)/keytool
ADB=$(SDK)/platform-tools/adb
FAIDL=$(SDK)/platforms/android-$(TARGET)/framework.aidl
AIDL=$(BUILDTOOLS)/aidl
CLASSPATH=$(AJAR):$(shell echo $(ls include 2> /dev/null) | sed s/ /:/g)

SRC=src/
NAME="dtrclock"

KEYFILE=keystore.jks

all: clear abuild build zipalign sign
build:
	mkdir bin || true
	mkdir gen || true
	$(AAPT) package -v -f -I $(AJAR) -M "AndroidManifest.xml" -A "assets" -S "res" -m -J "gen" -F "bin/resources.ap_"
	$(JAVAC) -classpath $(CLASSPATH) -sourcepath $(SRC) -sourcepath bin/aidl/ -sourcepath gen -d bin `find gen -name "*.java"` `find bin/aidl/ -name "*.java"` `find $(SRC) -name "*.java"`
	$(ADX) --dex --output=bin/classes.dex bin
	mv bin/resources.ap_ bin/$(NAME).ap_
	cd bin ; $(AAPT) add $(NAME).ap_ classes.dex
abuild:
	mkdir -p bin/aidl
	$(AIDL) -Iaidl -I$(SRC) -p$(FAIDL) -obin/aidl/ `find aidl -name "*.aidl"` `find $(SRC) -name "*.aidl"`
zipalign:
	$(ZIPALIGN) -v -p 4 bin/$(NAME).ap_ bin/$(NAME)-aligned.ap_
	mv bin/$(NAME)-aligned.ap_ bin/$(NAME).ap_
optimize:
	optipng -o7 $(shell find res -name *.png)
sign:
	$(APKSIGNER) sign --ks $(KEYFILE) --out bin/$(NAME).apk bin/$(NAME).ap_
	rm -f bin/$(NAME).ap_

clear:
	rm -rf bin gen
install:
	$(ADB) install -r bin/$(NAME).apk
